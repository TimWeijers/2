//Programmeermethoden 2016 - opgave 1: IQ-test
//Door: Daan Prinsze (s1910043) & Tim Weijers (s1887542)
//Beiden eerstejaars Wiskunde
//Docent: dr. W.A. Kosters
//Programma geschreven van maandag 12 - donderdag 22 september
//mailadres: wjstim@gmail.com & daanprinsze@gmail.com

//geschreven in gedit 3.18.3 op Ubuntu 16.04 LTS
//compileren getest met g++ (Ubuntu):
	//in Terminal: g++ -Wall -o naam naam.cc -std=c++11
//en Code:Blocks (Windows 8).
//tab size: 2 / 4

#include <iostream>
#include <string>
#include <stdio.h>
#include <cmath>
#include <cstdlib>
#include <stdlib.h>
#include <time.h>
using namespace std;

int main()
{
	srand ( time (NULL) );
	//intitialiseert randomgeneratoor met de UNIX-tijd
	//elke verstreken seconde een nieuwe seed

	const int pJ = 2016, pM = 9, pD = 23;
	//peildatum (jaar-maand-dag)
	int       gJ, gM, gD;
	//geboortedatum (jaar-maand-dag)
	const int minJr = 10, maxJr = 100;
	//minimale en maximale leeftijd
	int       maxLeeftijdMens = (pJ - 2016 + 116);
	//maximaal mogelijke leeftijd van een levend mens (in 2016 = 116)

	string maandNaam [13] {"index", "januari", "februari", "maart", "april",
                           "mei", "juni", "juli", "augustus", "september",
                           "oktober", "november", "december"};
	//door index op de eerste plek van de array is januari: maandnaam[1],
	//hierdoor hoeft de ingevoerde gM (geboortemaand) niet aangepast.

	cout << string(100, '\n')
	//scherm leegmaken, betrouwbaarder dan system("cls")

	//hieronder begint het infoblokje
			 << string (60, '=') << endl << endl
			 //dubble lijn

			 << " Programmeermethoden 2016 - opgave 1: IQ-test \n"

			 << string (60, '_') << endl
			 //enkele lijn

			 << "\n     Gemaakt op 17-9-2016 door:	\n\n"
			 << "Daan Prinsze\t & \tTim Weijers		\n"
			 << "s1910043   \t   \ts1877542 			\n"
			 << "Wiskunde   \t   \tWiskunde			\n\n"
			 << "Docent: dr. W.A. Kosters			\n"

			 << string (60, '=') << endl << endl << endl
			 //dubbele lijn
			 //einde van infoblokje

			 << "Welkom bij de IQ-test!\n"
			 << "Om de test af te mogen nemen moeten eerst een aantal vragen\n"
			 << "over uw geboortedatum beantwoord worden.\n\n"
			 << "Druk op Enter om te beginnen.\n";
	//einde cout
	cin.ignore(100, '\n');
	//verwijdert de Enter, (of een andere input) uit de stream

	cout << "Voer geboortejaar in (formaat: jjjj) en druk op ENTER.\n";
	cin >> gJ;
	//gebruiker voert geboortejaar in
		if (gJ < 0)
		{
			cout << "Invoer ongeldig!\n"
					 << "Leeftijd moet een positief getal zijn!\n";
			return 1;
		//als het geboortejaar < 0 dan stopt het programma met een error
		}
		else if (gJ > pJ)
		{
			cout << "Invoer ongeldig!\n"
					 << "Geboortejaar ligt in de toekomst.\n";
			return 1;
		//als het geboortejaar > het peiljaar stopt het programma met error
		}
		else if (pJ - gJ > maxLeeftijdMens)
		{
			cout << "Invoer ongeldig!\n"
					 << "In het jaar " << pJ << "kan een mens niet ouder dan "
					 << maxLeeftijdMens << " zijn. \n";
			return 1;
		//de oudste mens in 2016 is 116 jaar, als de gebruiker aangeeft
		//ouder te zijn stopt het programma met een error
		}
		else if (pJ - gJ < minJr)
		{
			cout << "Te jong voor dit programma!";
			return 1;
		//als de leeftijd van de gebruiker kleiner is dan 
		//de minimumleeftijd, stopt het programma met een error
		}
		else if (pJ - gJ > (maxJr + 1))
		{
			cout << "Te oud voor dit programma!";
			return 1;
		//maxJr + 1 want: als iemand in 1916 geboren is, is hij nu 99 of 100
		}


	cout << "Voer geboortemaand in (formaat: mm) en druk op ENTER.\n";
	cin >> gM;
	//gebruiker voert geboortemaand in
		if (gM <= 0 || gM > 12)
		{
			cout << "Invoer ongeldig!\n"
					 << "Voer een positieve integer met 1<=n<=12 in!\n";
	 		return 1;
		//als de gebruiker een ongeldig maandnummer invoert stopt
		//het programma met een error
		}
		else if (pJ - gJ == minJr && pM < gM)
		{
			cout << "Te jong voor dit programma!";
			return 1;
		//als pM < gM dan is de gebruiker nog niet jarig geweest
		//en dus nog niet de minimumleeftijd
		}
		else if (pJ - gJ == maxJr && pM > gM)
		{
		cout << "Te oud voor dit programma!";
		return 1;
		//als pM > gM is de gebruiker al jarig geweest
		//en dus ouder dan de maximumleeftijd
		}

	bool sJBoolG = (gJ % 4 == 0 && (gJ % 100 != 0 || gJ % 400 == 0));
	//sJBoolG is true (1) als het geboortejaar een schrikkeljaar is.
	//Elk jaar deelbaar door 4, maar niet door 100, 
	//tenzij deelbaar door 400 is een schrikkeljaar.
	
	int maandLengteG [13] = {0,31,28,31,30,31,30,31,31,30,31,30,31 };
	//Aantal dagen in een maand in een niet-schrikkeljaar
	//maandLengteG [0] is voor index-shift opdat de gM input direct
	//gebruikt kan worden.
	maandLengteG [2] += sJBoolG;
	//tel 1 dag op bij de lengte van februari als het geboortejaar
	//een schrikkeljaar is, zo niet, tel er dan 0 bij op.

	cout << "Voer geboortedag in (formaat:dd) en druk op ENTER.\n";
	cin >> gD;
	//gebruiker voert geboortedag in
		if (gD > maandLengteG [gM])
		{
			cout << "Invoer ongeldig!\n"
					 << "De maand " << maandNaam [gM]
					 << " heeft slechts " << maandLengteG [gM] << " dagen.\n";
			return 1;
		//als de waarde van de ingevoerde geboortedag groter is dan 
		//de lengte van de geboortemaand stopt het programma met een error
		}
		else if (gD < 1)
		{
			cout << "Invoer ongeldig!\n"
					 << "De geboortedag moet een positief getal >=1 zijn!\n";
			return 1;
		//als de gebruiker 0 of een negatief getal invoert stopt
		//het programma met een error
		}
		else if (pJ - gJ == minJr && pM == gM && pD < gD)
		{
			cout << "Te jong voor dit programma!";
			return 1;
		//als pD < gD is de gebruiker nog niet jarig geweest
		//en dus nog niet de minimumleeftijd
		}
		else if (pJ - gJ == maxJr && pM == gM && pD >= gD)
		{
			cout << "Te oud voor dit programma!";
			return 1;
		//als pD > gD is de gebruiker al jarig geweest
		//en dus ouder dan de maximumleeftijd
		}

	cout << string(100, '\n');
	//maakt het scherm leeg

	bool reedsJarigGeweest (pM > gM || (pM == gM && pD >= gD));
	//is true (1) als gebruiker in het peiljaar al jarig is geweest
	bool reedsMaandigGeweest (pD > gD);
	//is true (1) als de gebruiker in de peilmaand al maandig is geweest
	int	lJ, lM;
	//leeftijd in jaren, maanden
	lJ = (pJ - gJ - 1 + reedsJarigGeweest);
	//omwille van de leesbaarheid deze vergelijking
	//ipv de kortere: lJ = (pJ - gJ - (!reedsJarigGeweest))
	//of de nog kortere: lJ = (pJ - gJ - (nogNietJarigGeweest))

	if (reedsJarigGeweest)
		lM = ((pJ - gJ) * 12 + (pM - gM) - 1 + reedsMaandigGeweest);
		//Als iemand in het peiljaar al jarig is geweest,
		//dan is zijn leeftijd in maanden:
			//(pJ - gJ) * 12 -> 12 maanden per jaar.
			//+ (pM - gM) -> + aantal maanden sinds verjaardag.
			//(- 1) -> correctie want huidige maand telt niet mee voor totaal, tenzij
			//(+ reedsMaandigGeweest) -> de huidige maand wel meetelt

	else
		lM = ((pJ - gJ - 1) * 12 + (12 - gM) + (pM - 1) + reedsMaandigGeweest);
		//als iemand in het peiljaar nog niet jarig is geweest,
		//dan is zijn leeftijd in maanden:
			//((pJ - gJ - 1) * 12) -> (jaren oud - 1) want peiljaar telt niet mee.
			//(+(12 - gM)) -> maanden van geboortemaand tot eind v jaar VOOR peiljaar.
			//(+ pM - 1) -> van januari peiljaar tot (niet t/m) peilmaand.
			//(+ reedsMaandigGeweest) -> als iemand al maandig is geweest telt de
			//peilmaand mee voor het totaal.

	bool dertiger (lJ >= 30);
	//als iemand 30 jaar of ouder dan true, jonger dan 30 false
	string jijOfU [2] {"je", "u"};
	string psVnw  = jijOfU [dertiger];
	//Persoonlijk Voornaamwoord.
	//als dertiger == true (1) dan u
	//als dertiger == false (0) dan jij
	string jouwOfUw [2] {"jouw", "uw"};
	string bzVnw = jouwOfUw [dertiger];
	//Bezittelijk Voornaamwoord
	//als dertiger == true (1) dan uw
	//als dertiger == false (0) dan jouw

	if (pM == gM && pD == gD)
		cout << "Hartelijk gefeliciteerd met " << bzVnw
			 << " " << lJ << "-e verjaardag!\n\n";
	//gebruiker is jarig
        
	else if (pM != gM && pD == gD)
		cout << "Gefeliciteerd met " << bzVnw << " maandig zijn!\n\n";
	//gebruiker is maandig. Een gebruiker kan niet jarig én maandig
	//zijn: jarig zijn heeft voorrang

	cout << psVnw << " bent " << lJ << " jaar en " << lM - (12 * lJ)
			 << " maanden oud, " << lM << " maanden.\n\n";
	//print de leeftijd van de gebruiker in jaren en maanden


	const int mndDag[13][13] = {{0,0,31,59,90,120,151,181,212,243,273,304,334},
	                            {0,0,31,60,91,121,152,182,213,244,274,305,335}};
	//2-dimensionale array voor de dagen van 1/1/gJ tot 1/gM/gJ.
	//eerste rij is voor normaal jaar, tweede rij is voor schrikkeljaar.
	
	int dagenSinds;
	//totaal aantal verstreken dagen van 1/1/1900 tot gD/gM/gJ
	
	dagenSinds = (gJ - 1900) * 365
	//aantal dagen van 1/1/1900 tot 1/1/gJ
	+ mndDag[sJBoolG][gM]
	//aantal dagen van 1/1/gJ tot 1/gM/gJ
	+ (gD - 1)
	//aantal dagen van 1/gM/gJ  tot gD/gM/gJ
	+ ((gJ - 1900-1) / 4);
	//schrikkeldagen sinds 1900
	//let op! 1900 was GEEN schrikkeljaar! daarom -1

	int modDag = dagenSinds % 7;
	//geeft de restwaarde van dagenSinds / 7
	//gebruikt om de dag van de week te bepalen.
	string dagNaam [7] {"maandag", "dinsdag", "woensdag", "donderdag",
	                    "vrijdag", "zaterdag", "zondag"};
	//een array begint bij 0. Als modDag == 0, dan een maandag
	//omdat 1/1/1900 een maandag is (modDag == 0) begint de array met maandag.

	cout << "Geef de eerste letter van de dag van \nde week waarop "
		 << psVnw << " geboren bent.\n";

	static char letter [2];
	//een array waar de gebruiker de eerste 2 letters van de naam van zijn 
	//geboortedag input. static geeft initialisatiewaarde 0 aan de chars in 
	//de array. Dit is nodig omdat letter [1], in het geval van ma, wo, & vr 
	//geen input krijgt door de gebruiker.

	while (true)
	//loop net zolang tot letter een geldige input heeft
	{
	cin >> letter [0];
		if (letter [0] == 'z' || letter [0] == 'd')
		{
			cout << "geef de tweede letter\n";
			cin >> letter [1];
			//als de eerste letter een z of d is wordt de
			//gebruiker gevraagd de tweede letter in te voeren
			if (letter [1] == 'o' || letter [1] == 'a' || letter [1] == 'i')
				break;
			//stop loop en ga verder zodra geldige input
			else
				cout << "Invoer ongeldig! Probeer het opnieuw.\n"
 						 << "Geef de eerste letter\n";
			//als letter [1] een ongeldige input krijgt dan begint
			//de loop weer vanaf het begin en reset de input stream.
		}//einde if
		else if (letter [0] == 'm' || letter [0] == 'w' || letter [0] == 'v')
			break;
		//stop loop en ga verder in dit geval is een tweede letter 
		//niet nodig om de dag te bepalen
		else
		cout << "Invoer ongeldig! Probeer het opnieuw.\n"
				 << "Geef de eerste letter\n";
		//als letter [0] een ongeldige input krijgt dan begint
		//de loop weer vanaf het begin en reset de input stream.
		cin.clear();
		//reset de error flags in de stream
		cin.ignore(10000, '\n');
		//verwijder 10000 characters uit de stream
	}//einde while loop

	int wkDag = letter[0]+letter[1];
	//maakt van de som van de asci waarde
	//van de 2 letter inputs een int-waarde.
	//letter [1] is zonder gebruikersinvoer 0
	int wkNmr;
	//waarde van 0 t/m 6 correspondeert met maandag t/m zondag
	if      (wkDag == 109) wkNmr = 0;
	else if (wkDag == 205) wkNmr = 1;
	else if (wkDag == 119) wkNmr = 2;
	else if (wkDag == 211) wkNmr = 3;
	else if (wkDag == 118) wkNmr = 4;
	else if (wkDag == 219) wkNmr = 5;
	else if (wkDag == 233) wkNmr = 6;
	//maakt gebruik van de som van de ASCI waardes van
	//de karakters m, d, w, v, z, i, a, o.
	//bv: d + i == 205 > wkNmr 1 > dagNaam [1] > dinsdag

	cout << string(100, '\n');
	//maakt scherm leeg

	if (wkNmr == modDag)
	//de door de gebruiker ingevoerde weekdag is gelijk aan de 
	//door het programma gecalculeerde weekdag
		cout << "Correct! \n"
				 << "De IQ-test begint nu.\n";
	else
	{
		cout << "Fout!" << endl << bzVnw << " geboortedag viel op een "
				 << dagNaam [modDag] << endl << endl;
		return 1;
	//de door de gebruiker ingevoerde weekdag is niet gelijk aan de 
	//door het programma gecalculeerde. Het programma stopt met een error
	}

	//hieronder begint het vragendeel van de IQ-test
	int getalA = rand () % 10000;
	//eerste getal van te raden product
	int getalB = rand () % 10000;
	//tweede getal van te raden product
	int getalTienProcent = rand () % 10;
	//een willekeurig getal van 0 t/m 9
	//is in 10% van de gevallen gelijk aan 0
	if (getalTienProcent == 0) getalA = getalTienProcent;
	//in 10% van de gevallen wordt getal A gelijk aan 0
	//en dus het te raden product ook 0

	double getalAntwoord = (getalA * getalB);
	//het door het programma gecalculeerde product
	double getalUser;
	//de door de gebruiker geschatte waarde van het product

	char karel, tolstoy;
	//chars voor de 2 multiple-choice (A/B/C/D) vragen

		cout << string (60, '>') << endl;
		//lijn om het begin van het vragendeel aan te geven

		cout << "\nVraag 1:\nWat is het product van "<< getalA
			 << " en " << getalB << "?\n";
		cin >> getalUser;
		//de gebruiker raadt het product
		cout << getalA << " * " << getalB << " = " << getalAntwoord;
		//print het goede antwoord, door het programma gecalculeerd
		
	double afwijking = (getalAntwoord / getalUser);
	//de verhouding tussen het gegeven en het juiste antwoord
	double EPS = 0.1;
	//Epsilon: de toegestane afwijking tussen getalUser en
	//getalAntwoord. Hier 10%
		
		if ((getalAntwoord == 0 && getalUser == 0) || 
		(afwijking >= (1 - EPS) && afwijking <= (1 + EPS)))
		{
			cout << "\n\nCorrect! " << psVnw
				 << " wordt toegelaten tot een beta-studie!";
			return 0;
		//de afwijking ligt tussen (1 - EPS) en (1 + EPS) dus < 10%
		//het programma stopt zonder errors
		}
		else
		{
			cout << "\n\nFout! Niet getreurd, " << psVnw 
				 << " gaat door voor een\nplek bij een alfa-studie.\n"
				 << string (60, '.');
				 //lijn van punten.
		}

		if (dertiger)
		//gebruiker krijgt deze vraag als hij 30 of ouder is
		{
			cout << "Wie schreef het toneelstuk Karel ende Elegast?\n"
				 << "\tA: Harry Mulisch\n"
				 << "\tB: Multatuli\n"
				 << "\tC: Saskia Noort\n"
				 << "\tD: Schrijver is onbekend\n";
			//het juiste antwoord is D: Schrijver is onbekend

			cin >> karel;
				if (karel=='D' || karel=='d')
				{
					cout << "Correct! Succes met " << bzVnw
						 << "alfa-studie!!!\n\n\n";
					return 0;
				//gebruiker geeft juiste antwoord
				//programma stopt zonder errors
				}
				else if
				(karel == 'A' || karel == 'B' || karel == 'C' ||
				 karel == 'a' || karel == 'b' || karel == 'c')
				{
					cout << "Fout! \n Helaas, " << psVnw
						 << "wordt niet toegelaten aan de universiteit.\n"
						 << "Succes met " << bzVnw << "cursus op het HBO!";
					return 0;
				//gebruiker geeft foute antwoord,
				//programma stopt zonder errors
				}
		}//einde if (dertiger)

		if (!(dertiger))
		//gebruiker krijgt deze vraag als hij jonger dan 30 is
		{
			cout << "Wie schreef het boek Oorlog en Vrede?\n"
				 << "\tA: Leo Tolstoy\n"
				 << "\tB: Fyodor Dostoyevski\n"
				 << "\tC: Vladimir Nabokov\n"
				 << "\tD: Saskia Noort\n";
			//het juiste antwoord is A: Leo Tolsoy

	  	cin >> tolstoy;
				if (tolstoy=='A' || tolstoy=='a')
				{
					cout << "Correct! Succes met "<< bzVnw 
						 << " alfa-studie!\n\n\n\n";
					return 0;
				//gebruiker geeft juiste antwoord,
				//programma stopt zonder errors
				}
				else if
				(tolstoy=='B'|| tolstoy=='C'|| tolstoy=='D' || 
				 tolstoy=='b' || tolstoy=='c'|| tolstoy=='d')
				{
					cout << "Fout! \n Helaas, " << psVnw
						 << " wordt niet toegelaten aan de universiteit.\n"
						 << "Succes met " << bzVnw << " cursus op het HBO!";
					return 0;
				//gebruiker geeft foute antwoord,
				//programma stopt zonder errors
				}
		}//einde if (!(dertiger)
	return 0;
}//einde programma
